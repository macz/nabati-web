@extends('layout.layout')

@section('title')
Acerca de Nabati empresa guatemalteca
@endsection

@section('content')
    <div class="site-blocks-cover inner-page overlay" style="background-image: url(images/fresh-herb-falafel.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-5 text-center" data-aos="fade">
                <h1 class="text-uppercase">Acerca de nosotros</h1>
                <span class="caption d-block text-white">Nabati falafel croquetas de garbanzo</span>
            </div>
        </div>
    </div>

    <div class="slant-1"></div>



    <div class="site-half first-section">
        <div class="img-bg-1" style="background-image: url('images/nueva_presentacion.JPG');" data-aos="fade"></div>
        <div class="container">
            <div class="row no-gutters align-items-stretch">
                <div class="col-lg-5 ml-lg-auto py-5">
                    <span class="caption d-block mb-2 font-secondary font-weight-bold">El sabor del falafel llega a Guatemala</span>
                    <h2 class="site-section-heading text-uppercase font-secondary mb-5">Un poco de historia</h2>
                    <p>Nabati es una empresa guatemalteca que nace para satisfacer el mercado de productos veganos. Nuestra receta de croquetas de garbanzo es única. Contiene especias que caracterizan el sabor del oriente medio.</p>
                    <p>Nabati significa vegano y nuestros productos son realizados con ingredientes de primera calidad. 100% veganos.</p>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="site-half block">
        <div class="img-bg-1 right" style="background-image: url('images/atras.jpg');" data-aos="fade" data-aos-delay="100"></div>
        <div class="container">
            <div class="row no-gutters align-items-stretch">
                <div class="col-lg-5 mr-lg-auto py-5">
                    <span class="caption d-block mb-2 font-secondary font-weight-bold">Misión visión y valores</span>
                    <h2 class="site-section-heading text-uppercase font-secondary mb-5">Misión visión y valores</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus aliquid eius facilis voluptatem eligendi magnam accusamus vel commodi asperiores sint rem reprehenderit nobis nesciunt veniam qui fugit doloremque numquam quod.</p>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur tempora distinctio ipsam nesciunt recusandae repellendus asperiores amet.</p>
                </div>
            </div>
        </div>
    </div>--}}

    @include('bussiness_footer')
@endsection
