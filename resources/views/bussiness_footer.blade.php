<div class="py-5 bg-nabati-secondary mt-4">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6 text-center mb-3 mb-md-0">
                <h2 class="text-uppercase text-nabati mb-4" data-aos="fade-up">Oportunidad de negocio</h2>
                <a href="{{route('contact')}}" class="btn btn-bg-primary font-secondary text-uppercase" data-aos="fade-up" data-aos-delay="100">Contáctanos</a>
            </div>
        </div>
    </div>
</div>
