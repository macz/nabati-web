<div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div> <!-- .site-mobile-menu -->


<div class="site-navbar-wrap js-site-navbar bg-nabati">

    <div class="container">
        <div class="site-navbar bg-nabati">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <h2 class="mb-0 site-logo"><a href="{{ route('home') }}" class="font-weight-bold"><img class="logo" src="images/logo.png">Nabati Guatemala</a></h2>
                </div>
                <div class="col-8">
                    <nav class="site-navigation text-right" role="navigation">
                        <div class="container">
                            <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3 text-white"></span></a></div>

                            <ul class="site-menu js-clone-nav d-none d-lg-block">
                                <li class="{{ Route::currentRouteNamed('home') ? 'active' : ''}}"><a href="{{route('home')}}">Inicio</a></li>
                                <li class="{{ Route::currentRouteNamed('about') ? 'active' : ''}}"><a href="{{ route('about') }}">Acerca</a></li>
                                {{--                                    <li class="has-children">--}}
                                {{--                                        <a href="about.html">About</a>--}}
                                {{--                                        <ul class="dropdown arrow-top">--}}
                                {{--                                            <li><a href="testimonials.html">Testimonials</a></li>--}}
                                {{--                                            <li><a href="gallery.html">Gallery</a></li>--}}
                                {{--                                            <li><a href="faq.html">FAQ</a></li>--}}
                                {{--                                            <li><a href="why-choose-us.html">Why Choose Us</a></li>--}}
                                {{--                                            <li class="has-children">--}}
                                {{--                                                <a href="#">Sub Menus</a>--}}
                                {{--                                                <ul class="dropdown">--}}
                                {{--                                                    <li><a href="testimonials.html">Testimonials</a></li>--}}
                                {{--                                                    <li><a href="gallery.html">Gallery</a></li>--}}
                                {{--                                                    <li><a href="why-choose-us.html">Why Choose Us</a></li>--}}
                                {{--                                                </ul>--}}
                                {{--                                            </li>--}}
                                {{--                                        </ul>--}}
                                {{--                                    </li>--}}
                                <li class="{{ Route::currentRouteNamed('products') ? 'active' : ''}}"><a href="{{ route('products') }}">Productos</a></li>
                                <li class="{{ Route::currentRouteNamed('contact') ? 'active' : ''}}"><a href="{{ route('contact') }}">Contacto</a></li>
                                <!-- Social Info -->
                                <li><a href="https://www.facebook.com/Nabati-106969884179292" class="pb-2 pr-2 pl-0 ml-2"><span class="icon-facebook"></span></a></li>
                                <li><a href="https://www.instagram.com/nabati.gt/" class="p-2"><span class="icon-instagram"></span></a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
