<footer class="site-footer bg-nabati">
    <div class="container">


        <div class="row">
            <div class="col-md-4 mb-4 mb-md-0">
                <h3 class="footer-heading mb-4 text-white">Acerca de Nabati</h3>
                <p>Nabati croquetas o medallones de garbanzo, el sabor del oriente medio llega a Guatemala</p>
                <p><a href="{{route('about')}}" class="btn btn-primary bg-nabati text-white px-4">Leer más</a></p>
            </div>
            <div class="col-md-5 mb-4 mb-md-0 ml-auto">
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h3 class="footer-heading mb-4 text-white">Accesos rápidos</h3>
                        <ul class="list-unstyled">
                            <li><a href="{{route('home')}}">Inicio</a></li>
                            <li><a href="{{route('about')}}">Acerca</a></li>
                            <li><a href="{{route('products')}}">Productos</a></li>
                            <li><a href="{{route('contact')}}">Contacto</a></li>
                            <li><a href="#">Políticas del sitio</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h3 class="footer-heading mb-4 text-white">Búscanos en redes</h3>
                        <ul class="list-unstyled">
                            <li><a href="https://www.facebook.com/Nabati-106969884179292"><span class="icon-facebook"></span>&nbsp;Facebook</a></li>
                            <li><a href="https://www.instagram.com/nabati.gt/"><span class="icon-instagram"></span>&nbsp;Instagram</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
{{--        <div class="row pt-5 mt-5 text-center">--}}
{{--            <div class="col-md-12">--}}
{{--                <p>--}}
{{--                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->--}}
{{--                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>--}}
{{--                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->--}}
{{--                </p>--}}


{{--            </div>--}}

{{--        </div>--}}
    </div>
</footer>
