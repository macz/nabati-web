@extends('layout.layout')

@section('title')
Nuestra diversa gama de productos falafel
@endsection

@section('content')
<div class="site-blocks-cover inner-page overlay" style="background-image: url(images/fresh-herb-falafel.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-5 text-center" data-aos="fade">
            <h1 class="text-uppercase">Productos</h1>
            <span class="caption d-block text-white">Prueba nuestras deliciosos Falafel acompañados de sus salsas tradicionales</span>
        </div>
    </div>
</div>

<div class="slant-1"></div>



<div class="site-half first-section">
    <div class="img-bg-1" style="background-image: url('images/frente.jpg');" data-aos="fade"></div>
    <div class="container">
        <div class="row no-gutters align-items-stretch">
            <div class="col-lg-5 ml-lg-auto py-5">
                <span class="caption d-block mb-2 font-secondary font-weight-bold">El clásico</span>
                <h2 class="site-section-heading text-uppercase font-secondary mb-5">Falafel Estilo Mediterraneo</h2>
                <p>Deliciosas croquetas de garbanzo mezclado con especias del oriente medio, una opción rica y saludable, 100% Vegana.</p>
                <p>Acompáñalas en tus ensaladas, con pan pita y sus ricas salsas</p>
            </div>
        </div>
    </div>
</div>

<div class="site-half mt-5">
    <div class="img-bg-1" style="background-image: url('images/salsa_tatziki.JPG');" data-aos="fade"></div>
    <div class="container">
        <div class="row no-gutters align-items-stretch">
            <div class="col-lg-5 ml-lg-auto py-5">
                <span class="caption d-block mb-2 font-secondary font-weight-bold">Agrega sabor con salsas</span>
                <h2 class="site-section-heading text-uppercase font-secondary mb-5">Salsa Tzatziki</h2>
                <p>Deliciosa salsa hecha a base de yogurt, pepino y especias.</p>
                <p>La combinacón ideal para acompañar tus Falafel.</p>
            </div>
        </div>
    </div>
</div>

<div class="site-half mt-5">
    <div class="img-bg-1" style="background-image: url('images/salsa_vegana.JPG');" data-aos="fade"></div>
    <div class="container">
        <div class="row no-gutters align-items-stretch">
            <div class="col-lg-5 ml-lg-auto py-5">
                <span class="caption d-block mb-2 font-secondary font-weight-bold">Va con tu estilo</span>
                <h2 class="site-section-heading text-uppercase font-secondary mb-5">Salsa 100% Vegana</h2>
                <p>Deliciosa salsa hecha a base de maní 100% vegana.</p>
                <p>Dale un toque diferente a tus Falafel.</p>
            </div>
        </div>
    </div>
</div>

<div class="site-half mt-5">
    <div class="img-bg-1" style="background-image: url('images/combo.JPG');" data-aos="fade"></div>
    <div class="container">
        <div class="row no-gutters align-items-stretch">
            <div class="col-lg-5 ml-lg-auto py-5">
                <span class="caption d-block mb-2 font-secondary font-weight-bold">Disfruta en familia</span>
                <h2 class="site-section-heading text-uppercase font-secondary mb-5">Combos Nabati</h2>
                <p>Pregunta por nuestros combos, incluye Falafel, salsa y pan de pita.</p>
                <p>Ideal para disfrutar en familia.</p>
            </div>
        </div>
    </div>
</div>

@include('bussiness_footer')
@endsection
