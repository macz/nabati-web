@extends('layout.layout')
@section('title')
Bienvenido
@endsection

@section('content')
    <div class="slide-one-item home-slider owl-carousel">
        <div class="site-blocks-cover inner-page overlay" style="background-image: url(images/fresh-herb-falafel.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-6 text-center" data-aos="fade">
                        <h1 class="font-secondary">El sabor del oriente medio llega a Guatemala</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-blocks-cover inner-page overlay" style="background-image: url(images/pan-fried-falafel.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7 text-center" data-aos="fade">
                        <h1 class="font-secondary">Prueba nuestras croquetas listas para toda ocasión</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slant-1"></div>

    <div class="site-section first-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12 text-center" data-aos="fade">
                    <span class="caption d-block mb-2 font-secondary font-weight-bold">Listas para toda ocasión</span>
                    <h2 class="site-section-heading text-uppercase text-center font-secondary">Deliciosas croquetas falafel</h2>
                </div>
            </div>
            <div class="row border-responsive">
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0 border-right" data-aos="fade-up" data-aos-delay="">
                    <div class="text-center">
                        <span class="flaticon-bar-chart display-4 d-block mb-3 text-primary"></span>
                        <h3 class="text-uppercase h4 mb-3">Delicias del oriente medio</h3>
                        <p>El falafel es un plato a base de habas o garbanzos cocidos que se aderezan y se fríen en forma de croquetas.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0 border-right" data-aos="fade-up" data-aos-delay="100">
                    <div class="text-center">
                        <span class="flaticon-line-chart display-4 d-block mb-3 text-primary"></span>
                        <h3 class="text-uppercase h4 mb-3">Saludable Fuente de proteina</h3>
                        <p>No contiene azucar, colesterol o grasas trans. Es una fuente de proteina vegetal.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0 border-right" data-aos="fade-up" data-aos-delay="200">
                    <div class="text-center">
                        <span class="flaticon-box display-4 d-block mb-3 text-primary"></span>
                        <h3 class="text-uppercase h4 mb-3">Listas para comer</h3>
                        <p>Las croquetas congeladas estan listas para freir o calentar, sin complicaciones.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0" data-aos="fade-up" data-aos-delay="300">
                    <div class="text-center">
                        <span class="flaticon-medal display-4 d-block mb-3 text-primary"></span>
                        <h3 class="text-uppercase h4 mb-3">Diversidad de recetas</h3>
                        <p>Pruébalas con la tradicional salsa de yogurth, con pan de pita o en tus ensaladas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="site-half">
        <div class="img-bg-1" style="background-image: url('images/nueva_presentacion.jpg');" data-aos="fade"></div>
        <div class="container">
            <div class="row no-gutters align-items-stretch">
                <div class="col-lg-5 ml-lg-auto py-5">
                    <span class="caption d-block mb-2 font-secondary font-weight-bold">Listas para toda ocasión</span>
                    <h2 class="site-section-heading text-uppercase font-secondary mb-5">Nabati Croquetas de Garbanzo</h2>
                    <p>El falafel es un plato a base de habas o garbanzos cocidos que se aderezan y se fríen en forma de croquetas..</p>
                    <p>Las croquetas congeladas estan listas para freir o calentar, sin complicaciones.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-half block">
        <div class="img-bg-1 right" style="background-image: url('images/atras.jpg');" data-aos="fade"></div>
        <div class="container">
            <div class="row no-gutters align-items-stretch">
                <div class="col-lg-5 mr-lg-auto py-5">
                    <span class="caption d-block mb-2 font-secondary font-weight-bold">Cuida tu salud</span>
                    <h2 class="site-section-heading text-uppercase font-secondary mb-5">Una opción saludable</h2>
                    <p>No contiene azucar, colesterol o grasas trans. Es una fuente de proteina vegetal.</p>

                    <p>Pruébalas con la tradicional salsa de yogurth, con pan de pita o en tus ensaladas.</p>
                </div>
            </div>
        </div>
    </div>

    @include('bussiness_footer')
@endsection
