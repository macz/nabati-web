@extends('layout.layout')

@section('title')
Contacto
@endsection
@section('content')
<div class="site-blocks-cover inner-page overlay" style="background-image: url(images/fresh-herb-falafel.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-5 text-center" data-aos="fade">
            <h1 class="text-uppercase">Contáctanos</h1>
            @if(isset($send))
                <span class="caption d-block text-white bg-dark">Gracias por contactar con nosotros tu petición fue enviada</span>
            @else
                <span class="caption d-block text-white">Será un gusto poder atenderte</span>
            @endif
        </div>
    </div>
</div>

<div class="slant-1"></div>



<div class="site-section first-section" data-aos="fade">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-lg-8 mb-5">

                <form action="{{ route('contact') }}" method="POST" class="p-5 bg-white">
                    {{ csrf_field() }}
                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="fullname">Nombre</label>
                            <input type="text" name="name" id="fullname" class="form-control" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label class="font-weight-bold" for="email">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="phone">Teléfono</label>
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Teléfono #">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label class="font-weight-bold" for="message">Mensaje</label>
                            <textarea name="message" id="message" cols="30" rows="5" class="form-control" placeholder="Escribe tu mensaje"></textarea>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Enviar" class="btn btn-primary text-white px-4 py-2">
                        </div>
                    </div>


                </form>
            </div>

            <div class="col-lg-4">
                <div class="p-4 mb-3 bg-white">
                    <h3 class="h5 text-black mb-3">Contacto</h3>
                    <p class="mb-0 font-weight-bold">Dirección</p>
                    <p class="mb-4">20 calle 12-06 zona 10 (4,62 km), Ciudad de Guatemala</p>

                    <p class="mb-0 font-weight-bold">Teléfono</p>
                    <p class="mb-4"><a href="#">+502 4812-2418</a></p>

                    <p class="mb-0 font-weight-bold">Email</p>
                    <p class="mb-0"><a href="#">info@nabati.com.gt</a></p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
