<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        //
        $this->email = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->email->name.' : Nuevo email de contacto desde nabati.com.gt')
            ->from($this->email->email)
            ->to('info@nabati.com.gt')
            ->cc('ventas@nabati.com.gt')
            ->cc('agomez@nabati.com.gt')
            ->view('mail.contact', ['email' => $this->email]);
    }
}
