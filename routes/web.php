<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('contacto', function () {
    return view('contact');
})->name('contact');

Route::post('contacto', function (\Illuminate\Http\Request $request) {
    \Illuminate\Support\Facades\Mail::send(new \App\Mail\ContactMail($request));
    return redirect('contacto')->with('send', true);
})->name('contact');

Route::get('productos', function () {
    return view('products');
})->name('products');

Route::get('acerca', function () {
    return view('about');
})->name('about');
